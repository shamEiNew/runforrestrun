# RunForrestRun 🏃‍♀️🏃‍♂️🏃‍♀️🏃‍♂️
Welcome !

This repository is to maintain my progress and also list references as I go further of dwell into the subject.

### **Day 1**
Today I decided to start with a commitment to complete 300DaysOfCode and machine learning. I started with
linear regression, orthogonal projection, and its generalization to any inner-product space.

**Summary**
| Functions      | Description |
| ----------- | ----------- |
| Error Function      | $`RSS(\beta)=(Y-X\beta)^T(Y-X\beta)`$       |
| Projection Matrix   | $`X(X^TX)^{-1}X^T`$        |
| Estimation | $`\hat{y}=X(X^TX)^{-1}X^T\beta`$ |
|Univariate Regression (only two columns $`\hat{y}=\beta x+\epsilon`$)|$`\beta=\frac{\langle x, y \rangle}{\langle x, x \rangle}`$|
 
### **Day 2**
Today was the continuation of yesterday's topic. Topics included regression by successive orthogonalization, this is just the
*Gram-Schmidt Orthogonalization*. But this process also encompasses the correlation between the vectors as the residual error will be small and estimated coefficient will be unstable.

**Successive Orthogonalization**

`Initialize z0, x0=1, 1`

`For j = 1, 2, ... , p:`

`Regress xj on z0, z1, ...  zj-1`

`rlj = <zl, xl>/<zl, zl>, l = 0, ... , j-1`

`zj = xj - sum_{k=0}^{j-1} rlj zk`


**Summary**
|      |  |
| ----------- | ----------- |
| Esimated Coefficients      | $`\hat{\beta}_p=\frac{\langle z_p, y \rangle}{\langle z_p, z_p \rangle}`$   |

### **Day 3**

Today tried feature engineering on dataset available from kaggle and observed results in the [notebook](https://www.kaggle.com/shameinew/feature-engineering-tps-2).

*Mathematics*: Revisited some of the topics in ring theory, specifically on nilpotent elements, idempotent elemenst, rings
of the form $`Z_n[i]`$.

**Summary**

Optuna's graph of hypertuning
![Optuna Image](./images/newplot.png)

### **Day 4**

Topics learned today included Selection methods like Forward Stepwise Selection, Backward stepwise selection, forward stagewise selection. Further, studied shrinkage methods like ridge regression and lasso. Ridge regression involves invertible matrices so solving the problem of inverse in original regression. Also lasso is capable of making coefficients 0 compared to ridge because of the $`L_1`$ norm (but in this process we loose differentiability of the error function).

**Summary**
|      |  |
| ----------- | ----------- |
| Esimated Coefficient vector using ridge   | $`\hat{\beta}_{ridge}=(X^TX+\lambda I)^{-1}X^TY`$   |

### **Day 5**

*Deep Learning*: Gradient Descent Algorithm, Momentum based Gradient Descent algorithm. Contour maps of error surface.

**Summary**
|      |  |
| ----------- | ----------- |
|Momentum  | $`\text{ update}_{i}=\sum_{i=0}^{n}\lambda^{i-1} w_i`$   |
| Momentum based Gradient Descent algorithm  | $`w_i=w_i- \eta \text{ update}_{i-1}`$   |

### **Day 6**

Imputation methods on missing data using mean, median, regression.

### **Day 7**

*SQL*

Intermediate SQL inclusive of window functions.

*EDA*

EDA with Python, CDF, PDF, PMF, Measures of central tendency

*Methods*

Useful numpy methods:

`np.histogram`: Returns counts and bins.

`np.cumsum`: cumulative sum with respect to axis, default is the flattened array.






